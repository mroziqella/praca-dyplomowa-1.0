package pl.mroziqella.hermes.client.DesktopApp.model;

import javafx.application.Platform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mroziqella.hermes.client.DesktopApp.controller.LoginController;
import pl.mroziqella.hermes.client.core.client.listener.ToView.HomeView;
import pl.mroziqella.hermes.client.core.client.listener.ToView.LoggedView;
import pl.mroziqella.hermes.core.dto.UserDTO;

import java.util.logging.Logger;

@Component
public class LoggedViewImpl implements LoggedView{
     private Logger logger = Logger.getLogger(this.getClass().getName());

     @Autowired
     private LoginController loginController;

     @Autowired
     private HomeView homeView;



     public LoggedViewImpl() {

     }

     @Override
     public void logged(UserDTO userDTO) {
          if(userDTO.getEnabled()) {
               homeView.start();
          }else {
               Platform.runLater(() -> loginController.getInfo().setText("Błędne dane logowania!!!"));
          }
     }

}
