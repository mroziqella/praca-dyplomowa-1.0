package pl.mroziqella.hermes.client.DesktopApp;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import pl.mroziqella.hermes.client.DesktopApp.config.SpringFxmlLoader;
import pl.mroziqella.hermes.client.core.capture.ImageScreenShot;
import pl.mroziqella.hermes.client.core.client.Chat;


public class Main extends Application {
    public static Stage stage;
    private static double xOffset = 0;
    private static double yOffset = 0;

    @Override
    public void start(Stage primaryStage) throws Exception{
        SpringFxmlLoader loader = new SpringFxmlLoader();
        Parent root = loader.load(getClass().getResource("/view/LoginView.fxml"));
        draggedWindow(root,primaryStage);
        primaryStage.setTitle("Login");
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        Scene scene =new Scene(root, 600, 400);
        primaryStage.setScene(scene);
        primaryStage.show();

        stage=primaryStage;

        //Zdarzenie podczas wyłączania aplikacji
        Main.stage.setOnCloseRequest(event ->{
            SpringFxmlLoader.applicationContext.getBean(Chat.class).getSocketNamespace().values().forEach(r->r.disconnect().close());
            SpringFxmlLoader.applicationContext.getBean(Chat.class).getmSocket().disconnect().close();
            SpringFxmlLoader.applicationContext.getBean(ImageScreenShot.class).screenRunStop();
        });

    }

    public static void draggedWindow(Parent root,Stage primaryStage){
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
