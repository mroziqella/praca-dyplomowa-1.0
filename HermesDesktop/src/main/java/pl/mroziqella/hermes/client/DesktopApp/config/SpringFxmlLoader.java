package pl.mroziqella.hermes.client.DesktopApp.config;

import javafx.fxml.FXMLLoader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.mroziqella.hermes.client.core.config.Config;

import java.io.IOException;
import java.net.URL;

/**
 * Pobiera kontroler jako BEAN
 */
public class SpringFxmlLoader {

    public static final ApplicationContext applicationContext
            = new AnnotationConfigApplicationContext(Config.class);

    public <T> T load(URL url) {
        try  {
            FXMLLoader loader = new FXMLLoader(url);
            loader.setControllerFactory(applicationContext::getBean);
            return loader.load();
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }
}