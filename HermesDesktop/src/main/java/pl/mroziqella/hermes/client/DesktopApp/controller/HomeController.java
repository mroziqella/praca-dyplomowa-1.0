package pl.mroziqella.hermes.client.DesktopApp.controller;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import pl.mroziqella.hermes.client.DesktopApp.Main;
import pl.mroziqella.hermes.client.core.client.Chat;
import pl.mroziqella.hermes.core.until.MapperJSON;
import pl.mroziqella.hermes.core.config.ConstAppConfig;
import pl.mroziqella.hermes.core.dto.InfoDTO;
import pl.mroziqella.hermes.core.enumtype.TypeQuality;
import pl.mroziqella.hermes.core.enumtype.UserStatus;


import java.io.IOException;
import java.util.logging.Logger;

@Controller
public class HomeController {
    Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    private Chat chat;

    @FXML
    private ImageView imageView;
    @FXML
    private VBox roomBox;
    @FXML
    private VBox userBox;
    @FXML
    private Label lblRoomName;
    @FXML
    private TextField tbxZoomDowloandImage;
    @FXML
    private Label lblUserName;

    @FXML
    private ChoiceBox<TypeQuality> enumBoxQuality;

    @FXML
    private TextField tbxZoom;
    @FXML
    private Button btnStartTransmission;
    @FXML
    private TextField tbxSearchRoom;
    @FXML
    private Label lblInfo;
    @FXML
    private AnchorPane homePane;




    @FXML
    void startTransmission(ActionEvent event) throws IOException {


    }
    @FXML
    void btnMinimalize(ActionEvent event){
        Main.stage.setMaximized(false);
    }
    @FXML
    void btnSize(ActionEvent event){

    }
    @FXML
    void btnExit(ActionEvent event){
        System.exit(0);
    }

    @FXML
    void click(MouseEvent event) {

    }


    public void init() {
        InfoDTO infoDTO = new InfoDTO();
        infoDTO.setUserStatus(UserStatus.ACTIVE);
        chat.getmSocket().emit(ConstAppConfig.INIT, MapperJSON.toJSONObject(infoDTO));
    }

    public ChoiceBox<TypeQuality> getEnumBoxQuality() {
        return enumBoxQuality;
    }


    public TextField getTbxZoom() {
        return tbxZoom;
    }


    public VBox getRoomBox() {
        return roomBox;
    }

    public VBox getUserBox() {
        return userBox;
    }

    public Label getLblRoomName() {
        return lblRoomName;
    }

    public TextField getTbxZoomDowloandImage() {
        return tbxZoomDowloandImage;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public Label getLblUserName() {
        return lblUserName;
    }

    public TextField getTbxSearchRoom() {
        return tbxSearchRoom;
    }

    public Label getLblInfo() {
        return lblInfo;
    }

    public Button getBtnStartTransmission() {
        return btnStartTransmission;
    }

    public AnchorPane getHomePane() {
        return homePane;
    }
}