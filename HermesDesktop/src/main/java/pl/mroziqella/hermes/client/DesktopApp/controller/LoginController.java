package pl.mroziqella.hermes.client.DesktopApp.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import pl.mroziqella.hermes.client.core.service.UserService;
import pl.mroziqella.hermes.core.dto.UserDTO;

import java.util.logging.Logger;

/**
 * Created by Mroziqella on 21.12.2016.
 */
@Controller
public class LoginController {
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    private UserService userService;

    @FXML
    private Button btnSendLogin;

    @FXML
    private PasswordField lblPassword;

    @FXML
    private TextField lblLogin;

    @FXML
    private Label info;


    @FXML
    void btnSendLoginClick(ActionEvent event) {
        userService.singIn(UserDTO.Builder.userDTO()
                .withLogin(lblLogin.getText())
                .withPassword(lblPassword.getText())
                .build()
        );
    }
    @FXML
    void btnExit(ActionEvent event){
        System.exit(0);
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    public Button getBtnSendLogin() {
        return btnSendLogin;
    }

    public void setBtnSendLogin(Button btnSendLogin) {
        this.btnSendLogin = btnSendLogin;
    }

    public TextField getLblPassword() {
        return lblPassword;
    }

    public void setLblPassword(PasswordField lblPassword) {
        this.lblPassword = lblPassword;
    }

    public TextField getLblLogin() {
        return lblLogin;
    }

    public void setLblLogin(TextField lblLogin) {
        this.lblLogin = lblLogin;
    }

    public Label getInfo() {
        return info;
    }

    public void setInfo(Label info) {
        this.info = info;
    }
}
