<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="/resources/js/script.js"></script>
    <link rel="stylesheet" href="/resources/style/style.css">
    <meta charset="UTF-8">
    <title>Hermes</title>
</head>
<body>
<div class="background-image">
    <div class="container">
        <div class="login-container">
            <div id="output"></div>
            <div class="avatar">
                <a href="/" class="glyphicon glyphicon-home"></a>
            </div>
            <!--<div class="avatar"></div>-->
            <div class="form-box create">
                <input type="text" id="filter" onkeyup="filter()" placeholder="Szukaj">
                    <div style="margin-top:20px;overflow-y: scroll; height:40vh;">
                        <table class="table" id="tab">
                            <thead>
                            <tr>
                                <th>Nazwa</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${rooms}" var="room">
                                <tr>
                                   <td id="${rooms}"> <a href="transmission/${room.name}">${room.name}</a></td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>

    </div>
</div>
</body>
<script>
    $("input").click(function(){
        var status1 = $(this).prop('checked');
        var value = $(this).val();
        $.get("/api/user/status/"+value+"/"+status1, function(data, status){
        });
    });


</script>

</html>