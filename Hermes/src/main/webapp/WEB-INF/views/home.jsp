<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="/resources/style/style.css">
    <script language="javascript" type="text/javascript" src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <meta charset="UTF-8">

    <link rel="stylesheet" href="/resources/css/animation.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/resources/css/fontello-ie7.css"><![endif]-->

    <meta charset="UTF-8">
    <title>Hermes</title>
</head>
<body>
<div class="background-image">
    <div class="container">
        <div class="my-container">
            <div class="my-row">
                <a class="col-xs-4" href="room/all">
                    <i class="glyphicon glyphicon-eye-open">
                    </i><p>Transmisja</p>

                </a>
                <a class="col-xs-4" href="room/add">
                    <i class="glyphicon glyphicon-plus-sign">
                    </i><p>Dodaj pokój</p>

                </a>
                <a class="col-xs-4" href="/logout">
                    <i class="glyphicon glyphicon-off">
                    </i><p> Wyloguj</p>

                </a>
            </div>
            <div class="my-row">
                <sec:authorize access="hasAuthority('Admin')">
                    <a class="col-xs-4" href="users/all">
                        <i class="glyphicon glyphicon-user">
                        </i><p>Lista użytkowników</p>
                    </a>
                </sec:authorize>
                <sec:authorize access="hasAuthority('User')">
                    <a class="col-xs-4" href="users/edit">
                        <i class="glyphicon glyphicon-user">
                        </i><p>Edytuj konto</p>
                    </a>
                </sec:authorize>
                <a class="col-xs-4" href="/config">
                    <i class="glyphicon glyphicon-cog">
                    </i><p>Panel Administracyjny</p>

                </a>
                <a class="col-xs-4" href="room/add">
                    <i class="glyphicon glyphicon-download">
                    </i><p>Pobierz</p>

                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>