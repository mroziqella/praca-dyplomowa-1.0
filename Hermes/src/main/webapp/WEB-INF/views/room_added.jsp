

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="/resources/style/style.css">
    <meta charset="UTF-8">
    <title>Hermes</title>
</head>
<body>
<div class="background-image">
    <div class="container">
        <div class="login-container">
            <div id="output">Pokój utworzono!!!</div>

            <div class="avatar">
                <a href="/" class="glyphicon glyphicon-home"></a>
            </div>
            <div class="form-box">

            </div>
        </div>

    </div>
</div>
</body>
</html>