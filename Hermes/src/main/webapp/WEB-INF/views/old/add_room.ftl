<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring>
<meta charset="UTF-8" />
<form name="room" method="post">
<@spring.bind "room.*"/>
<#list spring.status.errorMessages as error> <b>${error}</b> <br> </#list>
    <p>

        <label for="name">Room name</label>
    <@spring.formInput "room.name"/>
    <@spring.showErrors "<br>"/>
    </p>
    <p>
        <label for="type">Typ</label>
    <@spring.formInput "room.type"/>
    </p>
    <p>
        <label for="password">Hasło</label>
    <@spring.formPasswordInput "room.password"/>
    </p>

<#list users as user>
    <p>${user.id}</p>
<p>${user.login}</p>
</#list>
    <button type="submit" class="btn">Zaloz</button>
</form>