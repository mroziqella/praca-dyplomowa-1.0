<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/resources/js/script.js"></script>

    <link rel="stylesheet" href="/resources/style/style.css">
    <meta charset="UTF-8">
    <title>Hermes</title>
</head>
<body>
<div class="background-image">
    <div class="container">
        <div class="login-container">
            <div id="output"></div>
            <div class="avatar">
                <a href="/" class="glyphicon glyphicon-home"></a>
            </div>
            <!--<div class="avatar"></div>-->
            <div class="form-box create">

                <input type="text" id="filter" onkeyup="filter()" placeholder="Szukaj">
                    <div style="overflow-y: scroll; height:40vh;">
                        <table class="table" id="tab">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Login</th>
                                <th>Aktywny</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${users}" var="user">
                                <tr>
                                    <td>${user.id}</td>
                                    <td>${user.login}</td>
                                    <td><input class="setStatus" type="checkbox" value="${user.login}" <c:if test="${user.enabled}">checked</c:if>></td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>

    </div>
</div>
</body>
<script>
    $(".setStatus").click(function(){
        var status1 = $(this).prop('checked');
        var value = $(this).val()
        $.get("/api/user/status/"+value+"/"+status1, function(data, status){
        });
    });
</script>

</html>