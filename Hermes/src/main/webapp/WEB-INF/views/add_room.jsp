<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="/resources/style/style.css">
    <meta charset="UTF-8">
    <title>Hermes</title>
</head>
<body>
<div class="background-image">
    <div class="container">
        <div class="login-container">
            <div id="output"></div>
            <div class="avatar">
                <a href="/" class="glyphicon glyphicon-home"></a>
            </div>
            <!--<div class="avatar"></div>-->
            <div class="form-box create">

                <form name="room" method="post">

                    <input type="text" name="name" id="name" placeholder="Nazwa pokoju"/>

                    <input type="text" name="password" id="password" placeholder="Hasło pokoju"/>
                    <select name="roomType" id="roomType" class="form-control">
                        <c:forEach items="${roomType}"  var="roomTyp">
                            <option value="${roomTyp}">${roomTyp}</option>
                        </c:forEach>
                    </select>
                    <select name="usersLogin" multiple class="form-control">
                        <c:forEach items="${users2}" var="user">
                            <option value="${user.login}">${user.login}</option>
                        </c:forEach>
                    </select>


                    <button class="btn btn-info btn-block login" type="submit">Załóż!</button>
                </form>
            </div>
        </div>

    </div>
</div>
</body>


</html>