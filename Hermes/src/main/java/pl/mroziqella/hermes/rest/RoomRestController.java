package pl.mroziqella.hermes.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.mroziqella.hermes.core.dto.RoomDTO;
import pl.mroziqella.hermes.service.RoomService;

import java.util.List;

/**
 * Created by Mroziqella on 01.01.2017.
 */
@RestController
@RequestMapping("/api/1/room")
public class RoomRestController {

    @Autowired
    private RoomService roomService;

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    public List<RoomDTO> getRooms(){
        return roomService.getAll();
    }
}
