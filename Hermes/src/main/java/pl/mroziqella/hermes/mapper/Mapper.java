package pl.mroziqella.hermes.mapper;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import pl.mroziqella.hermes.core.dto.RoomDTO;
import pl.mroziqella.hermes.core.dto.UserDTO;

import pl.mroziqella.hermes.entity.Room;
import pl.mroziqella.hermes.entity.User;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Mroziqella on 17.12.2016.
 */
public class Mapper {
    private static ModelMapper modelMapper = new ModelMapper();
    private static Logger logger = Logger.getLogger(Mapper.class.getName());
    public static UserDTO userToUserDTO(User user){
        return modelMapper.map(user,UserDTO.class);
    }




    @Deprecated
    public static Collection<UserDTO> userToUserDTO(Collection<User> users){
        Type listType = new TypeToken<Collection<UserDTO>>() {}.getType();
        return modelMapper.map(users,listType);
    }
    @Deprecated
    public static User userDTOtoUser(UserDTO userDTO){

        return modelMapper.map(userDTO,User.class);
    }
    @Deprecated
    public static RoomDTO roomToRoomDTO (Room room){
        return modelMapper.map(room,RoomDTO.class);
    }

    @Deprecated
    public static Collection<RoomDTO> roomToRoomDTO (Collection<Room> rooms){
        Type listType = new TypeToken<Collection<RoomDTO>>() {}.getType();
        return modelMapper.map(rooms,listType);
    }

    public static <T> T toDTO(Object object, Type type){
        return modelMapper.map(object,type);
    }

    public static <T> T toDTO(List<?> object,Type type){
        Type type2 = new ListParameterizedType(type);
        return modelMapper.map(object,type2);
    }

    public static <T> T fromDTO(Object object,Type type){
        return modelMapper.map(object,type);
    }
    public static <T> T fromDTO(List<?>  object,Type type){
        Type type2 = new ListParameterizedType(type);
        return modelMapper.map(object,type2);
    }


    private static class ListParameterizedType implements ParameterizedType {

        private Type type;
        private ListParameterizedType(Type type) {
            this.type = type;
        }

        @Override
        public Type[] getActualTypeArguments() {
            return new Type[] {type};
        }

        @Override
        public Type getRawType() {
            return Collection.class;
        }

        @Override
        public Type getOwnerType() {
            return null;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ListParameterizedType that = (ListParameterizedType) o;

            return type != null ? type.equals(that.type) : that.type == null;

        }

        @Override
        public int hashCode() {
            return type != null ? type.hashCode() : 0;
        }
    }
}
