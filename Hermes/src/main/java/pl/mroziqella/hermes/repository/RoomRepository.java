package pl.mroziqella.hermes.repository;

import org.springframework.transaction.annotation.*;
import pl.mroziqella.hermes.entity.*;

import java.util.List;

/**
 * Created by Kamil on 2016-10-10.
 */
public interface RoomRepository {
    @Transactional
    void add(Room room);
    List<Room> getAll();
    Room findByName(String name);
}
