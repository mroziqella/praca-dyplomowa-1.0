package pl.mroziqella.hermes.repository;

import org.springframework.transaction.annotation.Transactional;
import pl.mroziqella.hermes.entity.*;

import java.util.*;

/**
 * Created by Kamil on 2016-10-13.
 */
public interface UserRepository {
    @Transactional
    List<User> getAll();
    User findByLogin(String login);
    @Transactional
    void add(User user);
    @Transactional
    void edit(User user);
    @Transactional
    void setEnabeld(String login,boolean enabled);
}
