package pl.mroziqella.hermes.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.*;
import pl.mroziqella.hermes.entity.*;
import pl.mroziqella.hermes.repository.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Kamil on 2016-10-10.
 */
@Repository
public class IRoomRepository implements RoomRepository {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private UserRepository userRepository;

    @Override
    public void add(Room room) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user= userRepository.findByLogin(auth.getName());
        logger.info(Arrays.toString(room.getUsers().toArray()));
        if(room.getUsers()==null){
            ArrayList<User> users = new ArrayList<>();
            users.add(user);
            room.setUsers(users);
        }
        else {
            if(!room.getUsers().stream().anyMatch(r->r.getLogin().equals(user.getLogin()))){
                room.getUsers().add(user);
            }
        }
        logger.info(Arrays.toString(room.getUsers().toArray()));
        entityManager.persist(room);
        room.getUsers().forEach(r->{
            User byLogin = userRepository.findByLogin(r.getLogin());
            byLogin.getRooms().add(room);
            entityManager.persist(byLogin);
        });

    }

    @Override
    public List<Room> getAll() {
        return entityManager.createQuery("select r from Room r order by r.name",Room.class).getResultList();
    }

    @Override
    public Room findByName(String name) {
        return entityManager.createQuery("select r from Room r where r.name='"+name+"'",Room.class).getSingleResult();
    }
}
