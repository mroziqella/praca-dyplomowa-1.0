package pl.mroziqella.hermes.repository.impl;

import org.springframework.stereotype.*;
import pl.mroziqella.hermes.entity.*;
import pl.mroziqella.hermes.repository.*;

import javax.persistence.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by Kamil on 2016-10-13.
 */
@Repository
public class IUserRepository implements UserRepository {
    @PersistenceContext
    private EntityManager entityManager;

    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public List<User> getAll() {
        Query query = entityManager.createQuery("SELECT e from DUser e order by e.name");
        return query.getResultList();
    }

    @Override
    public User findByLogin(String login) {
        logger.info(login);
        Query query = entityManager.createQuery("SELECT e from DUser e where e.login = :login")
                .setParameter("login",login);
            User user = (User) query.getSingleResult();
            return user;

    }

    @Override
    public void add(User user) {
        Role role =entityManager.find(Role.class,2l);
        List<Role> roles = new ArrayList<>();
        roles.add(role);
        user.setRoles(roles);
        entityManager.persist(user);
    }

    @Override
    public void setEnabeld(String login, boolean enabled) {
        User user = findByLogin(login);
        user.setEnabled(enabled);
        entityManager.persist(user);
    }

    @Override
    public void edit(User user) {
        User user1 = findByLogin(user.getLogin());
        user.setId(user1.getId());
        user.setRoles(user1.getRoles());
        user.setRooms(user1.getRooms());
        user.setEnabled(user1.getEnabled());
        entityManager.merge(user);
    }
}
