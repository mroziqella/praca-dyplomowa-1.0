package pl.mroziqella.hermes.config.init;


import org.springframework.security.web.context.*;
import pl.mroziqella.hermes.config.SecurityConfig;


public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
    public SecurityWebApplicationInitializer() {
        super(SecurityConfig.class);
    }

}