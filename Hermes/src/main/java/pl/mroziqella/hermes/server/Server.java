package pl.mroziqella.hermes.server;

import com.corundumstudio.socketio.*;
import com.corundumstudio.socketio.listener.DataListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mroziqella.hermes.core.config.ConstAppConfig;
import pl.mroziqella.hermes.core.dto.InfoDTO;
import pl.mroziqella.hermes.core.dto.TransmissionDTO;
import pl.mroziqella.hermes.core.dto.UserDTO;
import pl.mroziqella.hermes.server.listener.TransmissionListener;
import pl.mroziqella.hermes.service.RoomService;
import pl.mroziqella.hermes.service.UserService;

import javax.annotation.PostConstruct;
import java.util.logging.Logger;



/**
 * Created by Kamil on 2016-10-13.
 */
@Component
public class Server {
    @Autowired
    private RoomService roomService;
    @Autowired
    private UserService userService;
    @Autowired
    private TransmissionListener transmissionListener;
    private int port = ConstAppConfig.PORT;

    private Logger logger = Logger.getLogger(Server.class.getName());
    private SocketIOServer server;
    private boolean run=true;

    @PostConstruct
    public void init(){
        Configuration config = new Configuration();
        config.setHostname(ConstAppConfig.HOST);
        config.setPort(port);
        config.getSocketConfig().setTcpSendBufferSize(3);
        config.getSocketConfig().setTcpReceiveBufferSize(3);
        config.setMaxHttpContentLength(6400*1024);
        config.setMaxFramePayloadLength(6400*1024);

        server = new SocketIOServer(config);
        server.startAsync();
        logger.info(">>>>>>>>>>>>>>>>>Start<<<<<<<<<<<<<<<<<<<<<");

        //Listener który słóży do logowania
        server.addEventListener(ConstAppConfig.LOGIN, UserDTO.class, new DataListener<UserDTO>() {
            @Override
            public void onData(SocketIOClient socketIOClient, UserDTO userDTO, AckRequest ackRequest) throws Exception {
                logger.info("próba logowania "+ userDTO + "\nKlientów " +socketIOClient.getNamespace().getAllClients().size());
                UserDTO  userDTO1 = userService.singIn(userDTO);
                socketIOClient.sendEvent(ConstAppConfig.LOGIN,userDTO1);
            }
        });

        //Listener do inicjalizacji
        server.addEventListener(ConstAppConfig.INIT, InfoDTO.class, new DataListener<InfoDTO>() {
            @Override
            public void onData(SocketIOClient socketIOClient, InfoDTO infoDTO2, AckRequest ackRequest) throws Exception {
                logger.info("--------INIT------------");
                InfoDTO infoDTO = new InfoDTO();
                infoDTO.setUserDTOList(userService.getAll());
                infoDTO.setRoomDTOList(roomService.getAll());
                socketIOClient.sendEvent(ConstAppConfig.INIT,infoDTO);

            }
        });
        roomService.getAll().forEach(r->createRoomNamespace(r.getName(),transmissionListener));
      //  server.getAllNamespaces().forEach(n->logger.info(n.getName()));
    }

    /**
     * Tworzy listener dla pokoju
     * @param namespace
     * @param dataListener
     */
    public void createRoomNamespace(String namespace,DataListener dataListener){
        server.addNamespace("/"+namespace).addEventListener(ConstAppConfig.TRANSMISSION, TransmissionDTO.class, dataListener);
    }

    public SocketIOServer getServer() {
        return server;
    }


    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
    public void stop(){
        if(server!=null) {
            server.stop();
            server = null;
        }
    }

    public boolean isRun() {
        return run;
    }

    public void setRun(boolean run) {
        this.run = run;
    }
}
