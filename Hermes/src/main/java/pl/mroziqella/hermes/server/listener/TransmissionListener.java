package pl.mroziqella.hermes.server.listener;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.DataListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mroziqella.hermes.core.config.ConstAppConfig;
import pl.mroziqella.hermes.core.dto.TransmissionDTO;
import pl.mroziqella.hermes.server.Server;

import java.util.Base64;
import java.util.logging.Logger;

/**
 * Created by Mroziqella on 23.12.2016.
 */
@Component
public class TransmissionListener implements DataListener<TransmissionDTO> {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    @Autowired
    private Server server;
    private SocketIOClient transmissionUser; //użytkownik który nadaje transmisje

    @Override
    public void onData(SocketIOClient socketIOClient, TransmissionDTO transmissionDTO, AckRequest ackRequest) throws Exception {
        if (checkTransmisionOrMouseEvent(transmissionDTO)) {
            transmissionUser = socketIOClient;//przypisuje osobę która nadaje obraz żeby później do niej móc wysłać współrzędne myszy
            sendImageToClients(socketIOClient, transmissionDTO);
        } else {
            transmissionUser.sendEvent(ConstAppConfig.TRANSMISSION, transmissionDTO);
        }

    }

    /**
     * Rozszyła do klientów, kązdy klient w osobynm wątku żeby się wzajemnie nie blokowali
     *
     * @param socketIOClient
     * @param transmissionDTO
     */
    private void sendImageToClients(SocketIOClient socketIOClient, TransmissionDTO transmissionDTO) {
        socketIOClient.getNamespace().getAllClients().stream().forEach(c -> new Thread(() -> {

            if (c.getHandshakeData().getHeaders().get("Accept-Encoding")!=null) {
                String s = Base64.getEncoder().encodeToString(transmissionDTO.getImage());
                c.sendEvent(ConstAppConfig.TRANSMISSION,s);
            } else{
                c.sendEvent(ConstAppConfig.TRANSMISSION, transmissionDTO);
            }
        }).start());
    }


    /**
     * sprawdza czy to jest transmisja czy sterowanie pulpitem
     *
     * @param transmissionDTO
     * @return
     */
    private boolean checkTransmisionOrMouseEvent(TransmissionDTO transmissionDTO) {
        return transmissionDTO.getMouseDTO() == null;
    }
}
