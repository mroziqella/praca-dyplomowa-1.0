package pl.mroziqella.hermes.restController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mroziqella.hermes.service.UserService;

import java.util.logging.Logger;

/**
 * Created by Mroziqella on 21.12.2016.
 */
@RestController("rest")
@RequestMapping("/api/user")
public class UserController {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    @Autowired
    private UserService userService;
    @RequestMapping("/status/{login}/{status}")
    public String setUserStatus(@PathVariable("login") String login,@PathVariable("status")boolean status){
        logger.info(login+status);
        userService.setEnabeld(login,status);
        return "hello";
    }
}
