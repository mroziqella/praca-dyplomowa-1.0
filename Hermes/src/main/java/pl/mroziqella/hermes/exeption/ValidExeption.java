package pl.mroziqella.hermes.exeption;

/**
 * Created by Kamil on 2016-10-10.
 */
public class ValidExeption extends RuntimeException {
    public ValidExeption() {
    }

    public ValidExeption(String message) {
        super(message);
    }

    public ValidExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidExeption(Throwable cause) {
        super(cause);
    }

    public ValidExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
