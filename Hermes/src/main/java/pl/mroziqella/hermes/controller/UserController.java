package pl.mroziqella.hermes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.mroziqella.hermes.core.dto.UserDTO;
import pl.mroziqella.hermes.service.UserService;

import javax.validation.Valid;

/**
 * Created by Mroziqella on 07.01.2017.
 */
@Controller
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/all")
    public String getAll(Model model){
        model.addAttribute("users",userService.getAll());
        return "user_list";
    }
    @RequestMapping("/edit")
    public String edit(Model model){
        model.addAttribute("user",userService.findByLogin(userService.getLoggedUser().getName()));
        return "user_add";
    }

    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    public String editPost(@ModelAttribute("user") @Valid UserDTO user){
        userService.edit(user);
        return "redirect:/";
    }
}
