package pl.mroziqella.hermes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.mroziqella.hermes.entity.User;
import pl.mroziqella.hermes.repository.UserRepository;

import java.util.logging.Logger;

/**
 * Created by Mroziqella on 23.12.2016.
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping("/test")
public class RestController {
    Logger logger = Logger.getLogger(this.getClass().getName());
    @Autowired
    private UserRepository userRepository;
    @RequestMapping
    public String test(){
        User user = userRepository.findByLogin("admin");
        logger.info(user.getEnabled()+"");
        return user.toString();
    }
}
