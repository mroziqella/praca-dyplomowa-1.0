package pl.mroziqella.hermes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.mroziqella.hermes.core.config.ConstAppConfig;
import pl.mroziqella.hermes.core.dto.Config;
import pl.mroziqella.hermes.core.dto.RoomDTO;
import pl.mroziqella.hermes.core.dto.RoomWithUserDTO;
import pl.mroziqella.hermes.core.enumtype.RoomType;
import pl.mroziqella.hermes.server.Server;
import pl.mroziqella.hermes.service.RoomService;
import pl.mroziqella.hermes.service.UserService;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Created by Kamil on 2016-10-10.
 */
@Controller
@RequestMapping("/room")
public class RoomController {

    private static Logger logger = Logger.getLogger(RoomController.class.getName());

    @Autowired
    private RoomService roomService;

    @Autowired
    private UserService userService;

    @Autowired
    private Server server;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addRoom(Model model) {
        model.addAttribute("room", new RoomWithUserDTO());
        model.addAttribute("users2", userService.getAll());
        model.addAttribute("roomType", RoomType.values());
        return "add_room";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addRoomPost(@ModelAttribute("room") @Valid RoomWithUserDTO room, BindingResult result, Model model) {

        if (result.hasErrors()) {
            logger.info(Arrays.toString(result.getAllErrors().toArray()));
            logger.info("error");
            model.addAttribute("roomType", RoomType.values());
            model.addAttribute("users2", userService.getAll());
            return "add_room";
        }
        roomService.add(room);
        return "redirect:/room/added";
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public String getAll(Model model) {
        model.addAttribute("rooms", roomService.getAll());
        return "room_list";
    }

    @RequestMapping("/added")
    public String roomAdded() {
        return "room_added";
    }

    @RequestMapping("/transmission/{roomName}")
    public String transmission(@PathVariable("roomName") String roomName, Model model) {
        String name = userService.getLoggedUser().getName();
        RoomDTO roomDTO = roomService.findByName(roomName);
        if(roomDTO.getType()==RoomType.PUBLIC.getRoomTypeValue()||userService.findByLogin(name).getRooms().stream().anyMatch(r->r.getName().equals(roomName))) {
            model.addAttribute("roomName", roomName);
            model.addAttribute("port",server.getPort());
            model.addAttribute("address", ConstAppConfig.HOST);
            return "transmission";
        }
        model.addAttribute("room",new RoomDTO());
        return "room_password";

    }
    @RequestMapping(value = "/transmission/{roomName}",method = RequestMethod.POST)
    public String transmissionPOST(@PathVariable("roomName") String roomName,@ModelAttribute("room") RoomDTO roomDTO, Model model) {
        if(roomService.findByName(roomName).getPassword().equals(roomDTO.getPassword())) {
            model.addAttribute("roomName", roomName);
            model.addAttribute("port",server.getPort());
            model.addAttribute("address", ConstAppConfig.HOST);
            return "transmission";
        }
        return "room_password";

    }
}
