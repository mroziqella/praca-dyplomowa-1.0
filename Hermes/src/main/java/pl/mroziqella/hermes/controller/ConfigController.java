package pl.mroziqella.hermes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.mroziqella.hermes.core.dto.Config;
import pl.mroziqella.hermes.server.Server;

import javax.validation.Valid;

/**
 * Created by Mroziqella on 23.02.2017.
 */
@Controller
@RequestMapping("/config")
public class ConfigController {

    @Autowired
    private Server server;

    @RequestMapping()
    public String config(Model model){
        Config config = new Config();
        config.setPort(server.getPort());
        config.setRun(server.isRun());
        model.addAttribute("config",config);
        return "config";
    }
    @RequestMapping(method = RequestMethod.POST)
    public String configPost(@ModelAttribute("config") @Valid  Config config){
        server.setPort(config.getPort());
        server.setRun(config.isRun());
        server.stop();
        if(config.isRun()) {
            server.init();
        }
        return "redirect:/";
    }

}
