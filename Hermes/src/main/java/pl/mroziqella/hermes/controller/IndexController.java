package pl.mroziqella.hermes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.mroziqella.hermes.core.dto.UserDTO;
import pl.mroziqella.hermes.service.UserService;

import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class IndexController {
    @Autowired
    private UserService userService;
    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public String login(){
        return "login";
    }
    @RequestMapping
    public String home(){
        return "home";
    }


    @RequestMapping(value = "/register",method = RequestMethod.GET)
    public String register(Model model){
        model.addAttribute("user",new UserDTO());
        return "user_add";
    }
    @RequestMapping(value = "/register",method = RequestMethod.POST)
    public String registerPost(@ModelAttribute("user") @Valid UserDTO user, BindingResult result, Model model){
        userService.add(user);
        return "redirect:/login";
    }


}