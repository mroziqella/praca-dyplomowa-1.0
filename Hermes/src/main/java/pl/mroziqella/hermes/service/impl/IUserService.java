package pl.mroziqella.hermes.service.impl;


import org.springframework.beans.factory.annotation.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.*;
import pl.mroziqella.hermes.core.dto.UserDTO;
import pl.mroziqella.hermes.core.until.Until;
import pl.mroziqella.hermes.entity.*;
import pl.mroziqella.hermes.mapper.Mapper;
import pl.mroziqella.hermes.repository.*;
import pl.mroziqella.hermes.server.Server;
import pl.mroziqella.hermes.service.*;

import java.util.*;
import java.util.logging.Logger;

/**
 * Created by Kamil on 2016-10-13.
 */
@Service
public class IUserService implements UserService {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private Server server;

    @Override
    public UserDTO singIn(UserDTO userDTO) {
        Authentication authToken = new UsernamePasswordAuthenticationToken(userDTO.getLogin(), userDTO.getPassword());
        UserDTO user1=null;
        try {
            userDTO.setEnabled(false);
            authToken = authenticationManager.authenticate(authToken);
            SecurityContextHolder.getContext().setAuthentication(authToken);
            logger.info(userDTO.getLogin()+" >>>>>LOGGIN SUCCESS<<<<<\n");
            User user = userRepository.findByLogin(userDTO.getLogin());
            user1=Mapper.toDTO(user,UserDTO.class);


        } catch (AuthenticationException e) {
            logger.info(userDTO.getLogin()+" >>>>>LOGGIN FAILED<<<<<");
        }catch (Exception e){
            e.printStackTrace();
        }
        logger.info(user1+"");
        return user1;
    }

    @Override
    public List<UserDTO> getAll() {
        //logger.info(Arrays.toString(new ArrayList<>(Mapper.userToUserDTO(userRepository.getAll())).toArray()));
        return new ArrayList<>(Mapper.userToUserDTO(userRepository.getAll()));
    }

    @Override
    public void setEnabeld(String login, boolean enabled) {
        userRepository.setEnabeld(login,enabled);
    }

    @Override
    public void add(UserDTO userDTO) {
        userDTO.setPassword(Until.hashPassword(userDTO.getPassword()));
        userRepository.add(Mapper.fromDTO(userDTO,User.class));
    }

    @Override
    public Authentication getLoggedUser() {
        return  SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public UserDTO findByLogin(String login) {
        return Mapper.toDTO(userRepository.findByLogin(login),UserDTO.class);
    }

    @Override
    public void edit(UserDTO user) {
        userRepository.edit(Mapper.fromDTO(user,User.class));
    }
}
