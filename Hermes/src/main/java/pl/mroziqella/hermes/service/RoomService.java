package pl.mroziqella.hermes.service;

import org.springframework.transaction.annotation.Transactional;
import pl.mroziqella.hermes.core.dto.RoomWithUserDTO;
import pl.mroziqella.hermes.core.dto.RoomDTO;

import java.util.List;

/**
 * Created by Kamil on 2016-10-10.
 */
public interface RoomService {
     void add(RoomWithUserDTO room);
     @Transactional
     List<RoomDTO> getAll();
     RoomDTO findByName(String name);

}
