package pl.mroziqella.hermes.service;

import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;
import pl.mroziqella.hermes.core.dto.UserDTO;

import java.util.*;

/**
 * Created by Kamil on 2016-10-13.
 */
public interface UserService {
    @Transactional
    List<UserDTO> getAll();
    UserDTO findByLogin(String login);
    UserDTO singIn(UserDTO userDTO);
    void add(UserDTO userDTO);
    void setEnabeld(String login,boolean enabled);
    Authentication getLoggedUser();
    void edit(UserDTO user);
}
