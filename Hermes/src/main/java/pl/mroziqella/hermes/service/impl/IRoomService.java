package pl.mroziqella.hermes.service.impl;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import pl.mroziqella.hermes.core.dto.RoomDTO;
import pl.mroziqella.hermes.core.dto.RoomWithUserDTO;
import pl.mroziqella.hermes.core.dto.UserDTO;
import pl.mroziqella.hermes.entity.*;
import pl.mroziqella.hermes.exeption.*;
import pl.mroziqella.hermes.mapper.Mapper;
import pl.mroziqella.hermes.repository.*;
import pl.mroziqella.hermes.server.Server;
import pl.mroziqella.hermes.server.listener.TransmissionListener;
import pl.mroziqella.hermes.service.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Kamil on 2016-10-10.
 */
@Service
public class IRoomService implements RoomService{

    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private Server server;
    @Autowired
    private TransmissionListener transmissionListener;

    @Override
    public void add(RoomWithUserDTO room) {
        if(room.getName().isEmpty()){
            throw new ValidExeption("Nie podano nazwy pokoju");
        }
        if(room.getUsersLogin()==null){
            room.setUsersLogin(new ArrayList<>());
        }
        room.setUsers(room.getUsersLogin().stream().map(r->UserDTO.Builder.userDTO().withLogin(r).build()).collect(Collectors.toList()));
        room.setRegisterDate(new Date());
        room.setType(room.getRoomType().getRoomTypeValue());
        roomRepository.add(Mapper.fromDTO(room,Room.class));
        server.createRoomNamespace(room.getName(),transmissionListener);
    }

    @Override
    public List<RoomDTO> getAll() {
        return new ArrayList<>(Mapper.roomToRoomDTO(roomRepository.getAll()));
    }

    @Override
    public RoomDTO findByName(String name) {
        return Mapper.toDTO(roomRepository.findByName(name),RoomDTO.class);
    }
}

