import io.socket.emitter.*;
import org.testng.*;
import pl.mroziqella.hermes.core.until.MapperJSON;
import pl.mroziqella.hermes.core.dto.InfoDTO;
import pl.mroziqella.hermes.core.enumtype.UserStatus;

import java.util.Arrays;
import java.util.logging.*;

/**
 * Created by Kamil on 2016-10-13.
 */
public class Listener implements Emitter.Listener {
    Logger logger = Logger.getLogger(Listener.class.getName());
    @Override
    public void call(Object... args) {
        InfoDTO infoDTO = MapperJSON.toObject(args[0],InfoDTO.class);
        logger.info(Arrays.toString(infoDTO.getRoomDTOList().toArray()));
        Assert.assertEquals(infoDTO.getUserStatus(), UserStatus.ACTIVE);
    }
}
