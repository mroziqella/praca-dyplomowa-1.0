package mapper;





import org.testng.annotations.Test;
import org.testng.collections.Lists;

import pl.mroziqella.hermes.core.dto.RoomDTO;
import pl.mroziqella.hermes.core.dto.UserDTO;
import pl.mroziqella.hermes.entity.Room;
import pl.mroziqella.hermes.entity.User;
import pl.mroziqella.hermes.mapper.Mapper;


import java.util.ArrayList;

import java.util.List;

import static org.testng.Assert.assertEquals;

/**
 * Created by Mroziqella on 17.12.2016.
 */

public class MapperTest {


    @Test
    public void userToUserDto() {
        Room role = new Room();
        role.setName("Admin");
        Room role2 = new Room();
        role2.setName("User");
        User user = new User();
        user.setLogin("Mroziqella");
        user.setEnabled(true);
        user.setName("Kamil");
        user.setPassword("AlaMaKota123");
        List<Room> rooms = new ArrayList<>();
        rooms.add(role);
        rooms.add(role2);
        user.setRooms(rooms);
        UserDTO userDTO = Mapper.toDTO(user,UserDTO.class);
        assertEquals(user.getLogin(), userDTO.getLogin());
        assertEquals(user.getRooms().get(0).getName(), userDTO.getRooms().get(0).getName());

    }

    @Test
    public void userDtoToUser() {
        RoomDTO role = new RoomDTO();
        role.setName("Admin123");
        RoomDTO role2 = new RoomDTO();
        role2.setName("User123");
        UserDTO user = new UserDTO();
        user.setLogin("Mroziqella1");
        user.setEnabled(true);
        user.setName("Kamil1");
        user.setPassword("AlaMaKota123");
        List<RoomDTO> rooms = new ArrayList<>();
        rooms.add(role);
        rooms.add(role2);
        user.setRooms(rooms);
        User userDTO = Mapper.fromDTO(user,User.class);
        assertEquals(user.getLogin(), userDTO.getLogin());
        assertEquals(user.getRooms().get(0).getName(), userDTO.getRooms().get(0).getName());

    }

    @Test
    public void toDTO() {
        RoomDTO role = new RoomDTO();
        role.setName("Admin123");
        RoomDTO role2 = new RoomDTO();
        role2.setName("User123");
        UserDTO user = new UserDTO();
        user.setLogin("Mroziqella1");
        user.setEnabled(true);
        user.setName("Kamil1");
        user.setPassword("AlaMaKota123");
        List<RoomDTO> rooms = new ArrayList<>();
        rooms.add(role);
        rooms.add(role2);
        user.setRooms(rooms);
        User kamil = new User(2l, "Kamil", "123", "321", true, null, Mapper.fromDTO(rooms,Room.class));

        UserDTO o = Mapper.toDTO(kamil, UserDTO.class);
        User u = Mapper.fromDTO(o,User.class);
        assertEquals(u,kamil);
    }
    @Test
    public void toDTOs() {
        List<User> users = Lists.newArrayList();
        users.add(new User(2l,"Kamil","123","321",true,null,null));
        users.add(new User(3l,"Ono","123","321",true,null,null));
        List<UserDTO> userDTOs = Mapper.toDTO(users,UserDTO.class);

        assertEquals(users,Mapper.fromDTO(userDTOs,User.class));

    }
}
