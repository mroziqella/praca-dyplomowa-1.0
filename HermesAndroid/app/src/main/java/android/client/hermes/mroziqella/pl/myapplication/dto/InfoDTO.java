package android.client.hermes.mroziqella.pl.myapplication.dto;


import android.client.hermes.mroziqella.pl.myapplication.enumtype.UserStatus;


import java.util.List;

/**
 * Słóży do przesyłania danych podczasn np inicjalizacji
 * Created by Kamil on 2016-10-13.
 */
public class InfoDTO{
    private UserStatus userStatus;
    private UserDTO userDTO;
    private List<UserDTO> userDTOList;
    private List<RoomDTO> roomDTOList;

    public InfoDTO() {
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public InfoDTO(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }


    public List<RoomDTO> getRoomDTOList() {
        return roomDTOList;
    }

    public void setRoomDTOList(List<RoomDTO> roomDTOList) {
        this.roomDTOList = roomDTOList;
    }

    public List<UserDTO> getUserDTOList() {
        return userDTOList;
    }

    public void setUserDTOList(List<UserDTO> userDTOList) {
        this.userDTOList = userDTOList;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InfoDTO infoDTO = (InfoDTO) o;

        if (userStatus != infoDTO.userStatus) return false;
        if (userDTO != null ? !userDTO.equals(infoDTO.userDTO) : infoDTO.userDTO != null) return false;
        if (userDTOList != null ? !userDTOList.equals(infoDTO.userDTOList) : infoDTO.userDTOList != null) return false;
        return roomDTOList != null ? roomDTOList.equals(infoDTO.roomDTOList) : infoDTO.roomDTOList == null;

    }

    @Override
    public int hashCode() {
        int result = userStatus != null ? userStatus.hashCode() : 0;
        result = 31 * result + (userDTO != null ? userDTO.hashCode() : 0);
        result = 31 * result + (userDTOList != null ? userDTOList.hashCode() : 0);
        result = 31 * result + (roomDTOList != null ? roomDTOList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InfoDTO{" +
                "userStatus=" + userStatus +
                ", userDTO=" + userDTO +
                ", userDTOList=" + userDTOList +
                ", roomDTOList=" + roomDTOList +
                '}';
    }
}
