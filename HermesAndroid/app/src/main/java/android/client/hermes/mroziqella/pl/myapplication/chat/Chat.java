package android.client.hermes.mroziqella.pl.myapplication.chat;

import android.client.hermes.mroziqella.pl.myapplication.chat.listener.Initialization;
import android.client.hermes.mroziqella.pl.myapplication.chat.listener.LoggedStatus;
import android.client.hermes.mroziqella.pl.myapplication.config.ConstAppConfig;

import org.json.JSONException;


import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Mroziqella on 09.01.2017.
 */

public class Chat {
    private Socket mSocket;
    public static  String currenrtRoomName;
    public static Chat getInstance;
    private static Map<String,Socket> socketNamespace = new HashMap<>();

    public Chat() {
        try {
            connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        getInstance=this;
    }

    public void connect() throws URISyntaxException, JSONException, InterruptedException {
        mSocket = IO.socket("http://" + ConstAppConfig.HOST + ":" + ConstAppConfig.PORT);
        mSocket.on(ConstAppConfig.LOGIN, new LoggedStatus());
        mSocket.on(ConstAppConfig.INIT, new Initialization());
        mSocket.connect();
    }

    public void createRoomNamespace(String name, Emitter.Listener listener){
        try {
            Socket s = IO.socket("http://" + ConstAppConfig.HOST + ":" + ConstAppConfig.PORT+"/"+name);
            // .connect();
            s.on(ConstAppConfig.TRANSMISSION,listener);
            socketNamespace.put(name,s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Socket getmSocket() {
        return mSocket;
    }

    public static Map<String, Socket> getSocketNamespace() {
        return socketNamespace;
    }
}
