package android.client.hermes.mroziqella.pl.myapplication.dto;


import android.client.hermes.mroziqella.pl.myapplication.enumtype.RoomType;



import java.util.Date;

/**
 * Created by Mroziqella on 17.12.2016.
 */
public class RoomDTO {
    private Long id;
    private String name;
    private String password;
    private Integer type;
    private Date registerDate;
    private RoomType roomType;


    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }


    @Override
    public String toString() {
        return "RoomDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", type=" + type +
                ", registerDate=" + registerDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoomDTO roomDTO = (RoomDTO) o;

        if (id != null ? !id.equals(roomDTO.id) : roomDTO.id != null) return false;
        if (name != null ? !name.equals(roomDTO.name) : roomDTO.name != null) return false;
        if (password != null ? !password.equals(roomDTO.password) : roomDTO.password != null) return false;
        if (type != null ? !type.equals(roomDTO.type) : roomDTO.type != null) return false;
        return registerDate != null ? registerDate.equals(roomDTO.registerDate) : roomDTO.registerDate == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (registerDate != null ? registerDate.hashCode() : 0);
        return result;
    }
}
