package android.client.hermes.mroziqella.pl.myapplication.chat.listener;

import android.client.hermes.mroziqella.pl.myapplication.DispalyActivity;
import android.client.hermes.mroziqella.pl.myapplication.dto.TransmissionDTO;
import android.client.hermes.mroziqella.pl.myapplication.dto.UserDTO;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.socket.emitter.Emitter;

import org.json.JSONObject;


import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by Mroziqella on 23.12.2016.
 */
public class TransmissionListener implements Emitter.Listener {
    @Override
    public void call(Object... args) {
        byte[] decodedString = Base64.decode(args[0].toString(), Base64.DEFAULT);
        final Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        DispalyActivity.appCompatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DispalyActivity.imageView.setImageBitmap(decodedByte);
            }

        });
    }
}
