package android.client.hermes.mroziqella.pl.myapplication.chat.listener;

import android.app.Activity;
import android.client.hermes.mroziqella.pl.myapplication.DispalyActivity;
import android.client.hermes.mroziqella.pl.myapplication.MainActivity;
import android.client.hermes.mroziqella.pl.myapplication.MenuActivity;
import android.client.hermes.mroziqella.pl.myapplication.chat.Chat;
import android.client.hermes.mroziqella.pl.myapplication.config.ConstAppConfig;
import android.client.hermes.mroziqella.pl.myapplication.dto.InfoDTO;
import android.client.hermes.mroziqella.pl.myapplication.dto.RoomDTO;
import android.client.hermes.mroziqella.pl.myapplication.dto.UserDTO;
import android.client.hermes.mroziqella.pl.myapplication.enumtype.RoomType;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class Initialization implements Emitter.Listener {

    private ArrayAdapter<String> adapter;


    @Override
    public void call(Object... objects) {
        JSONObject data = (JSONObject) objects[0];
        ObjectMapper mapper = new ObjectMapper();
        Log.i(">>INIT", "...........INIT COMPLETE...........");
        InfoDTO infoDTO = null;
        try {
            infoDTO = mapper.readValue(data.toString(), InfoDTO.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i(">>INIT", "...........INIT COMPLETE...........");
        ArrayList<String> strings = new ArrayList<>();
        for (RoomDTO r : infoDTO.getRoomDTOList()) {
            strings.add(r.getName());
            createRoomNamespace(r.getName(), new TransmissionListener());
        }

        adapter = new ArrayAdapter<String>(MainActivity.appCompatActivity.getApplicationContext(), android.R.layout.simple_list_item_1, strings);
        adapter.notifyDataSetChanged();

        MenuActivity.appCompatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MenuActivity.listView.setAdapter(adapter);
            }
        });
        final InfoDTO finalInfoDTO = infoDTO;
        MenuActivity.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                startGetTransmission(finalInfoDTO.getRoomDTOList().get(position));

            }
        });

    }


    private void startGetTransmission(RoomDTO r) {
        if (LoggedStatus.getCurrentUser().getRooms().contains(r)) {//logowanie do pokoju do którego ma sie dostęp
           openSocket(r.getName());
        } else if (r.getType().equals(RoomType.PUBLIC.getRoomTypeValue())) {//logowanie do pokoju publicznego
            createRoomNamespace(r.getName(), new TransmissionListener());
            openSocket(r.getName());
        }
        else {
            Toast.makeText(MenuActivity.appCompatActivity, "Brak uprawnień!!!", Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent(MenuActivity.appCompatActivity, DispalyActivity.class);
        MenuActivity.appCompatActivity.startActivity(intent);
    }

    private void openSocket(String roomName) {
        for (Socket r: Chat.getSocketNamespace().values()){
            r.disconnect().off();
        }

        Chat.getSocketNamespace().get(roomName).connect().on(ConstAppConfig.TRANSMISSION,new TransmissionListener());
        Chat.currenrtRoomName=roomName;
    }

    public void createRoomNamespace(String name, Emitter.Listener listener) {
        try {
            IO.Options options = new IO.Options();

            Socket s = IO.socket("http://" + ConstAppConfig.HOST + ":" + ConstAppConfig.PORT + "/" + name,options);
            // .connect();
            s.on(ConstAppConfig.TRANSMISSION, listener);
            Chat.getSocketNamespace().put(name, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}