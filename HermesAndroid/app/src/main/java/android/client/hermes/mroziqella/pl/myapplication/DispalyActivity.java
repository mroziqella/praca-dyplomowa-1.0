package android.client.hermes.mroziqella.pl.myapplication;

import android.client.hermes.mroziqella.pl.myapplication.chat.Chat;
import android.client.hermes.mroziqella.pl.myapplication.config.ConstAppConfig;
import android.client.hermes.mroziqella.pl.myapplication.dto.MouseButton;
import android.client.hermes.mroziqella.pl.myapplication.dto.MouseDTO;
import android.client.hermes.mroziqella.pl.myapplication.dto.TransmissionDTO;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class DispalyActivity extends AppCompatActivity {
    public static AppCompatActivity appCompatActivity;
    public static ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispaly);
        imageView = (ImageView) findViewById(R.id.imageView);
        appCompatActivity = this;
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        Gson gson = new Gson();
                        try {
                            TransmissionDTO transmissionDTO = new TransmissionDTO();
                            transmissionDTO.setMouseDTO(new MouseDTO((int) event.getX(), (int) event.getY(), MouseButton.PRIMARY));
                            JSONObject obj = new JSONObject(gson.toJson(transmissionDTO));
                            Chat.getSocketNamespace().get(Chat.currenrtRoomName).emit(ConstAppConfig.TRANSMISSION, obj);
                            Log.v("test", obj.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                }
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        Chat.getSocketNamespace().get(Chat.currenrtRoomName).disconnect().off();
        finish();
        return;
    }
}
