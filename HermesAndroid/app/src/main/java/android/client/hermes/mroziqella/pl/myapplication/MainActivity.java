package android.client.hermes.mroziqella.pl.myapplication;

import android.client.hermes.mroziqella.pl.myapplication.chat.Chat;
import android.client.hermes.mroziqella.pl.myapplication.config.ConstAppConfig;
import android.client.hermes.mroziqella.pl.myapplication.dto.UserDTO;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    private Chat chat;
    public static AppCompatActivity appCompatActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appCompatActivity=this;
        chat = new Chat();
        setContentView(R.layout.activity_main);
        Button button  = (Button)findViewById(R.id.btnLogin);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText login = (EditText)findViewById(R.id.login);
                EditText password = (EditText)findViewById(R.id.password);
                JSONObject object = new JSONObject();
                try {
                    object.put("login", login.getText());
                    object.put("password",password.getText());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                chat.getmSocket().emit(ConstAppConfig.LOGIN, object);
            }
        });





    }
}
