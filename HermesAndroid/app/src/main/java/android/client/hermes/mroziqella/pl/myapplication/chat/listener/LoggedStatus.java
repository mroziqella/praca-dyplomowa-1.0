package android.client.hermes.mroziqella.pl.myapplication.chat.listener;


import android.client.hermes.mroziqella.pl.myapplication.MainActivity;
import android.client.hermes.mroziqella.pl.myapplication.MenuActivity;
import android.client.hermes.mroziqella.pl.myapplication.chat.Chat;
import android.client.hermes.mroziqella.pl.myapplication.config.ConstAppConfig;
import android.client.hermes.mroziqella.pl.myapplication.dto.UserDTO;
import android.content.Intent;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.io.IOException;

import io.socket.emitter.Emitter;

public class LoggedStatus implements Emitter.Listener {
    private static UserDTO currentUser;
    @Override
    public void call(Object... args) {

        JSONObject data = (JSONObject) args[0];
        ObjectMapper mapper = new ObjectMapper();
        UserDTO userDTO = null;
        try {
            userDTO = mapper.readValue(data.toString(), UserDTO.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(MainActivity.appCompatActivity, MenuActivity.class);
        MainActivity.appCompatActivity.startActivity(intent);


        Chat.getInstance.getmSocket().emit(ConstAppConfig.INIT);
        currentUser=userDTO;
    }

    public static UserDTO getCurrentUser() {
        return currentUser;
    }

}