package android.client.hermes.mroziqella.pl.myapplication.enumtype;

/**
 * Created by Mroziqella on 31.12.2016.
 */
public enum RoomType {
    PUBLIC,
    PRIVATE;

    public int getRoomTypeValue() {
        switch (this) {
            case PRIVATE:
                return 1;
            default:
                return 0;
        }
    }
}
