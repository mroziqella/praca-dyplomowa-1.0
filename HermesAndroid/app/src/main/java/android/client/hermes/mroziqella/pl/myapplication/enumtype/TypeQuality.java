package android.client.hermes.mroziqella.pl.myapplication.enumtype;

/**
 * Created by Mroziqella on 29.12.2016.
 */
public enum TypeQuality {
    HIGH,
    MEDIUM,
    LOW;

    public float getQualityValue() {
        switch (this) {
            case HIGH:
                return  0.7f;
            case MEDIUM:
                return  0.5f;
            default:
                return  0.1f;
        }
    }
}
