package android.client.hermes.mroziqella.pl.myapplication.config;

/**
 * Created by Kamil on 2016-10-10.
 */
public class ConstAppConfig {

    public static final String HOST = "192.168.1.100";
    public static final int PORT = 9093;
    public static final String LOGIN="login";
    public static final String INIT="init";
    public static final String ROOM="room";
    public static final String TRANSMISSION="transmision";
    public static Integer TYPE_PRESENTATION = 1;
    public static Integer TYPE_FULL = 2;

}
