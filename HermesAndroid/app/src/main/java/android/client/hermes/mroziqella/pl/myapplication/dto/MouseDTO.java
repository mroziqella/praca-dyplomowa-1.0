package android.client.hermes.mroziqella.pl.myapplication.dto;

import android.client.hermes.mroziqella.pl.myapplication.dto.MouseButton;



/**
 * Created by Mroziqella on 03.01.2017.
 */
public class MouseDTO {
    private int x;
    private int y;
    private MouseButton mouseKey;

    public MouseDTO() {
    }

    public MouseDTO(int x, int y, MouseButton mouseKey) {
        this.x = x;
        this.y = y;
        this.mouseKey = mouseKey;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public MouseButton getMouseKey() {
        return mouseKey;
    }

    public void setMouseKey(MouseButton mouseKey) {
        this.mouseKey = mouseKey;
    }

    @Override
    public String toString() {
        return "MouseDTO{" +
                "x=" + x +
                ", y=" + y +
                ", mouseKey=" + mouseKey +
                '}';
    }
}
