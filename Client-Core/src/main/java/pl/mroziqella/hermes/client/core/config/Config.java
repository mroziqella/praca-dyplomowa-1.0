package pl.mroziqella.hermes.client.core.config;



import io.socket.client.IO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import io.socket.client.Socket;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.TestingAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import pl.mroziqella.hermes.core.config.ConstAppConfig;

import java.net.URISyntaxException;

/**
 * Created by Kamil on 22/08/2016.
 */
@Configuration
@ComponentScan(basePackages = {"pl.mroziqella.hermes.client",
"pl.mroziqella.hermes.client.DesktopApp"})
public class Config {
    @Bean
    public AuthenticationProvider authenticator() {
        return new TestingAuthenticationProvider();
    }

    @Bean
    public ConstAppConfig constAppConfig(){
        return new ConstAppConfig();
    }
}
