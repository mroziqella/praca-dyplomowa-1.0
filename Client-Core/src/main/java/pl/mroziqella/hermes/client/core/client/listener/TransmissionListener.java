package pl.mroziqella.hermes.client.core.client.listener;

import io.socket.emitter.Emitter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mroziqella.hermes.client.core.client.listener.ToView.HomeView;
import pl.mroziqella.hermes.core.until.MapperJSON;
import pl.mroziqella.hermes.core.dto.TransmissionDTO;


import java.util.logging.Logger;

/**
 * Created by Mroziqella on 23.12.2016.
 */
@Component
public class TransmissionListener implements Emitter.Listener{
    private Logger logger = Logger.getLogger(this.getClass().getName());
    @Autowired
    private HomeView homeView;

    @Override
    public void call(Object... objects) {
        homeView.downloadTransmission(MapperJSON.toObject(objects[0], TransmissionDTO.class));
    }
}
