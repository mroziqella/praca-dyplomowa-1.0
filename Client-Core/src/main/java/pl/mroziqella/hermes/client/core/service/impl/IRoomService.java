package pl.mroziqella.hermes.client.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mroziqella.hermes.client.core.client.Chat;
import pl.mroziqella.hermes.client.core.client.listener.TransmissionListener;
import pl.mroziqella.hermes.core.config.ConstAppConfig;


/**
 * Created by Mroziqella on 28.12.2016.
 */
@Service
public class IRoomService{
    @Autowired
    private Chat chat;
    @Autowired
    private TransmissionListener transmissionListener;

    public String roomName;

    public void openSocket(String roomName) {
        chat.getSocketNamespace().values().forEach(r->r.disconnect().off());
        chat.getSocketNamespace().get(roomName).connect().on(ConstAppConfig.TRANSMISSION,transmissionListener);
        this.roomName=roomName;
    }
    public void closeSocket(String roomName){
        chat.getSocketNamespace().values().forEach(r->r.disconnect().off());
    }
}
