package pl.mroziqella.hermes.client.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mroziqella.hermes.client.core.client.Chat;
import pl.mroziqella.hermes.client.core.service.Emitter;
import pl.mroziqella.hermes.core.until.MapperJSON;
import pl.mroziqella.hermes.core.config.ConstAppConfig;
import pl.mroziqella.hermes.core.dto.MouseDTO;
import pl.mroziqella.hermes.core.dto.TransmissionDTO;


import java.util.logging.Logger;

/**
 * Wysyła wiadomości
 * Created by Mroziqella on 23.12.2016.
 */
@Service
public class IEmiter implements Emitter {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    @Autowired
    private Chat chat;

    @Override
    public void image(String roomName,byte[] image) {
        chat.getSocketNamespace()
                .get(roomName)
                .emit(ConstAppConfig.TRANSMISSION, MapperJSON.toJSONObject(new TransmissionDTO(image)));
    }

    @Override
    public void click(String roomName, MouseDTO mouseDTO) {
        logger.info("coord: "+ mouseDTO);
        chat.getSocketNamespace()
                .get(roomName)
                .emit(ConstAppConfig.TRANSMISSION, MapperJSON.toJSONObject(new TransmissionDTO(mouseDTO)));
    }

    @Override
    public void keyClick(String roomName, String key) {
        chat.getSocketNamespace()
                .get(roomName)
                .emit(ConstAppConfig.TRANSMISSION, MapperJSON.toJSONObject(new TransmissionDTO(key)));
    }
}
