package pl.mroziqella.hermes.client.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import pl.mroziqella.hermes.client.core.client.Chat;
import pl.mroziqella.hermes.client.core.service.UserService;
import pl.mroziqella.hermes.core.until.MapperJSON;
import pl.mroziqella.hermes.core.config.ConstAppConfig;
import pl.mroziqella.hermes.core.dto.UserDTO;


import java.util.logging.Logger;

/**
 * Created by Mroziqella on 21.12.2016.
 */
@Service
public class IUserService implements UserService {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    @Autowired
    private Chat chat;
    private UserDTO currentUser;

    @Override
    public void singIn(UserDTO userDTO) {
        chat.getmSocket().emit(ConstAppConfig.LOGIN, MapperJSON.toJSONObject(userDTO));
    }

    @Override
    public UserDTO getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(UserDTO currentUser) {
        this.currentUser=currentUser;
    }
}
