package pl.mroziqella.hermes.client.core.service;


import pl.mroziqella.hermes.core.dto.UserDTO;

/**
 * Created by Mroziqella on 21.12.2016.
 */
public interface UserService {
    void singIn(UserDTO userDTO);
    UserDTO getCurrentUser();
    void setCurrentUser(UserDTO currentUser);
}
