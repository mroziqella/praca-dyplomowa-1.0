package pl.mroziqella.hermes.client.core.exeption;

/**
 * Created by Mroziqella on 05.01.2017.
 */
public class TransmissionAccessDetain extends Exception {
    public TransmissionAccessDetain() {
    }

    public TransmissionAccessDetain(String message) {
        super(message);
    }

    public TransmissionAccessDetain(String message, Throwable cause) {
        super(message, cause);
    }

    public TransmissionAccessDetain(Throwable cause) {
        super(cause);
    }

    public TransmissionAccessDetain(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
