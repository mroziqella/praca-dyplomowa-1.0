package pl.mroziqella.hermes.client.core.capture;

import javafx.scene.input.MouseButton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mroziqella.hermes.core.dto.MouseDTO;


import java.awt.*;
import java.awt.event.InputEvent;
import java.util.logging.Logger;


/**
 * Created by Mroziqella on 03.01.2017.
 */
@Component
public class Mouse {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private Robot robot;
    @Autowired
    private ImageScreenShot imageScreenShot;

    public Mouse() {
        try {
            robot = new Robot();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Kliknięcie myszą u transmitujacego
     */
    public void mouseClick(MouseDTO mouseDTO) {
        double zoom = 100. / imageScreenShot.getZoom();
        int button;
        logger.info(mouseDTO.getMouseKey().toString());
        if(mouseDTO.getMouseKey().equals(MouseButton.PRIMARY)){
            button = InputEvent.BUTTON1_MASK;
        }
        else {
            button =InputEvent.BUTTON3_MASK;
        }
        robot.delay(20);
        robot.mouseMove((int) (mouseDTO.getX() * zoom), (int) (mouseDTO.getY() * zoom));
        robot.delay(20);
        robot.mousePress(button);
        robot.delay(20);
        robot.mouseRelease(button);
    }
}
