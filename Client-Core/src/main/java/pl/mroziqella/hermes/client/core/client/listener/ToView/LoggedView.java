package pl.mroziqella.hermes.client.core.client.listener.ToView;


import pl.mroziqella.hermes.core.dto.UserDTO;

/**
 * Created by Mroziqella on 21.12.2016.
 */
public interface LoggedView {

    /**
     * Po próbie logowania użytkownika
     * @param userDTO zalogowany użytkownik
     */
    void logged(UserDTO userDTO);
}
