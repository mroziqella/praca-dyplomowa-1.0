package pl.mroziqella.hermes.client.core.client.listener;

import io.socket.emitter.Emitter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mroziqella.hermes.client.core.client.listener.ToView.LoggedView;
import pl.mroziqella.hermes.client.core.service.UserService;
import pl.mroziqella.hermes.core.until.MapperJSON;
import pl.mroziqella.hermes.core.dto.UserDTO;


import java.util.logging.Logger;

/**
 * Słuchacz loguje użytkownika
 * Created by Mroziqella on 18.12.2016.
 */
@Component
public class LoggedStatus implements Emitter.Listener {
    private Logger logger = Logger.getLogger(String.valueOf(this.getClass().getName()));

    @Autowired
    private LoggedView loggedView;

    @Autowired
    private UserService userService;

    @Override
    public void call(Object... objects) {
        UserDTO userDTO = MapperJSON.toObject(objects[0], UserDTO.class);
        loggedView.logged(userDTO);
        if(isUser(userDTO)) {
            userService.setCurrentUser(userDTO);
        }
    }
    private boolean isUser(UserDTO userDTO){
        return userDTO.getEnabled();
    }
}
