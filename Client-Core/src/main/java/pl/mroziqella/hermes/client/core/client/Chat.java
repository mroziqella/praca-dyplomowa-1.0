package pl.mroziqella.hermes.client.core.client;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import org.json.JSONException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mroziqella.hermes.client.core.client.listener.Initialization;
import pl.mroziqella.hermes.client.core.client.listener.LoggedStatus;
import pl.mroziqella.hermes.core.config.ConstAppConfig;


import javax.annotation.PostConstruct;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by Kamil on 2016-10-13.
 */
@Component
public class Chat {
    private Logger logger = Logger.getLogger(this.getClass().getName());

    private Socket mSocket;
    private Map<String,Socket> socketNamespace = new HashMap<>();

    @Autowired
    private LoggedStatus loggedStatus;

    @Autowired
    private Initialization initialization;

    public Chat() {
        logger.info("CREATE BEAN");

    }
    @PostConstruct
    public void connect() throws URISyntaxException, JSONException, InterruptedException {
        mSocket = IO.socket("http://" + ConstAppConfig.HOST + ":" + ConstAppConfig.PORT);
        mSocket.on(ConstAppConfig.LOGIN, loggedStatus);
        mSocket.on(ConstAppConfig.INIT, initialization);
        mSocket.connect();
    }

    public void createRoomNamespace(String name, Emitter.Listener listener){
        try {
            Socket s = IO.socket("http://" + ConstAppConfig.HOST + ":" + ConstAppConfig.PORT+"/"+name);
                   // .connect();
            s.on(ConstAppConfig.TRANSMISSION,listener);
            socketNamespace.put(name,s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public Socket getmSocket() {
        return mSocket;
    }

    public Map<String, Socket> getSocketNamespace() {
        return socketNamespace;
    }
}
