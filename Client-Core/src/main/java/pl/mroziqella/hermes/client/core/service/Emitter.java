package pl.mroziqella.hermes.client.core.service;


import pl.mroziqella.hermes.core.dto.MouseDTO;

/**
 * Wysyła obiekty na serwer
 * Created by Mroziqella on 23.12.2016.
 */
public interface Emitter {
     /**
      * Przesyła obraz
      * @param roomName
      * @param image
      */
     void image(String roomName,byte[] image);

     /**
      * Przesyła zdarzenie myszy
      * @param roomName
      * @param mouseDTO
      */
     void click(String roomName,MouseDTO mouseDTO);

     void keyClick(String roomName,String key);


}
