package pl.mroziqella.hermes.client.core.client.listener.ToView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mroziqella.hermes.client.core.client.Chat;
import pl.mroziqella.hermes.client.core.client.listener.TransmissionListener;
import pl.mroziqella.hermes.core.*;
import pl.mroziqella.hermes.core.dto.InfoDTO;
import pl.mroziqella.hermes.core.dto.RoomDTO;
import pl.mroziqella.hermes.core.dto.TransmissionDTO;

import java.util.List;
import java.util.logging.Logger;

/**
 * Metody służące do wyświetlania modelu
 * Created by Mroziqella on 18.12.2016.
 */
@Component
public abstract class HomeView {
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    public Chat chat;
    @Autowired
    public TransmissionListener transmissionListener;


    /**
     * Inicjalizuje okno główne
     * @param infoDTO
     */
    public abstract void downloadInitData(InfoDTO infoDTO);

    /**
     * Inicjalizuje kontroler, tutaj sie uruchamia nowe okno lub nadpisuje stare
     */
    public abstract void start();

    /**
     * Obsługuje odebrane informacje z transmisji
     * @param transmissionDTO
     */
    public abstract void downloadTransmission(TransmissionDTO transmissionDTO);

    /**
     * Inicjuje słuchaczy dla przekazanych pokoi
     * @param roomDTOs
     */
    public void initRoomNamespace(List<RoomDTO> roomDTOs){
        roomDTOs.forEach(r->chat.createRoomNamespace(r.getName(),transmissionListener));
    }
}
