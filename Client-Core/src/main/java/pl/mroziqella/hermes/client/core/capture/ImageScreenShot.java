/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mroziqella.hermes.client.core.capture;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mroziqella.hermes.client.core.exeption.TransmissionAccessDetain;
import pl.mroziqella.hermes.client.core.service.Emitter;
import pl.mroziqella.hermes.client.core.service.UserService;
import pl.mroziqella.hermes.client.core.service.impl.IRoomService;
import pl.mroziqella.hermes.core.enumtype.TypeQuality;


import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;
import javax.xml.bind.DatatypeConverter;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Kamil
 */
@Component
public class ImageScreenShot {
    @Autowired
    private Emitter emitter;
    @Autowired
    private IRoomService roomService;
    @Autowired
    private UserService userService;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    private byte[] imageByteArray;
    private Robot robot;
    private Dimension dimension;
    private Rectangle rectangle;
    private BufferedImage screen;
    public boolean screenRun = false;
    private int zoomValue = 50;
    private TypeQuality typeQuality = TypeQuality.HIGH;
    private int time = 1000;


    /**
     * Robienie zrzutow wstrzymane
     */
    public void screenRunStop() {
        this.screenRun = false;
    }


    /**
     * Ustawienie jaka jakośc obrazu ma być przesyłana
     *
     * @param qualityProcent jakość w procentach
     */
    public void setZoom(int qualityProcent) {
        this.zoomValue = qualityProcent;
    }

    public int getZoom() {
        return zoomValue;
    }

    private boolean checkAccess() {
        return userService.getCurrentUser()
                .getRooms()
                .stream()
                .noneMatch(r -> r.getName()
                        .equals(roomService.roomName));
    }

    /**
     * Watek tworzacy zrzuty ekranu
     */
    public void run() throws TransmissionAccessDetain {
        if (checkAccess()) {
            throw new TransmissionAccessDetain("Brak uprawnień do transmitowania!");
        }
        dimension = Toolkit.getDefaultToolkit().getScreenSize();
        rectangle = new Rectangle(dimension);
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        new Thread(() -> {
            screenRun = true;
            while (screenRun) {
                screenCapture();
                emitScreen();
            }
            // TODO: 05.01.2017 Zamienić żeby wyświetlał sie jakiś ładny obrazek że transmisja zakończona
            String data = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMwAAADMCAMAAAAI/LzAAAAAhFBMVEX///8AAAD+/v4BAQH7+/sFBQXw8PDs7Ozp6enl5eX19fXy8vJxcXGwsLAWFhZiYmJqamoxMTE/Pz+lpaU6OjqGhoaNjY1HR0eAgIC6urrb29vBwcFdXV2fn58mJibOzs5XV1fW1tYdHR14eHhPT08lJSWWlpbIyMgQEBBEREShoaEtLS12ECItAAASz0lEQVR4nO1d6WLqKhBOCNEkGvctWvVqPa227/9+NzMDrgkMNLb90Tm3vVUj8LHMDgTBH/3RH/3RHz2SgP+EoL/w5/5j+hH05O8mxFH+Rjq/vvrs8tHNZ7+P1JhIY1MVEBno8fmtJKTudD2HhMxarXaStFutTKq31Gflo/IXY8F2KjRZuivy9Wz62h0M+i8v/cGg+zqdrfNil2Yay29fNdC6zrKY9eeTTe8YPtCxt5nM+7Ni2Ql+GZSrrqX/y9ZyNTvEjxgeKT7MVsuWvPry/Z/fTLCAkTXhQpbL1bq/4QDRtOmvV0uJjEMX9FNggCFJqflski8Oe2hhxIWCD+4Pizyljin5gdSs8NuxXHWmXHWHuECicoqxZhk+FiOe43Cwys4C6Ie4NaIofzq72ZC6Ooqgv5lDgw9G6uHhbNehsf6hiYYyRSRFt6eQlC2DX9yROX8BX/a6RYKDLX8EDHRh++Nlj0gIDDaPDebcATRA+5ePdvD9/EyznfZ6Ujl/WBRVvJys21T8N0IilTdZD3nNdqHhOiE14rvQSJjY7fy/kM25uATF/Ze3qYbvIei0VX8PdcdsqcKhcrmVcPb9VfCd8yxd4JDEzWIBNFTuIv02LK0cFgtJlUbnWRypYsNh3nomgnJFShr85QAQkMRremRCkrslrsEuwOrkE8SoBHYMv2R+Ch3kvCek8vcpBxwSpEDDzABYJdpU6SsOyxOhhLr8+BU0UJEFjfPpspfK0ZHvE6ip6XVfgSYGPJP3ssoA53eThMa9zMZDtUCfOja6hij8HGeyeVcBrsRsuj+rYc+DooAQL9hPM813GqSyxPSAUOJnD40unyo6pEGT04xMJrkdPav1ZjptZSAaUtYEmsaBKEYOFnFzVFY5KpA9ywYmmyC/Y7Ep1aYfAANwPgvdii8TarA5FPt8jvxIVGfekB4NfEyOe9BDDev7TDSApzeWjfA0WHjjf4pZfj8WqLRUpf+Nv+rqkAHK3yAHj4XWAI010zO4uKLwrIkii6Uf/ScOc8Rh8OqhXo68yFcbQEcJmLAF26CMUDjEClTNl2LVvhgfZg52WVYhCI7fACEWIYpPtoSMVDMvjTxuJqP54b+Xl/8O89Fkc7yAjvXT3II/C2RofqoNqPzloIKsZLMx8h3RdOyNBrNxsdrulmmaJGm63G1XxXg2GJGb7fwwr+ByaEbbsjnSl0MDlnQeauuPCQb/mCzyXdp67EUhW+kuX0xun+aUW/6ep9gkr5FBG+YA48LlyWSGDPvj5aWQ4MocuUz35bg/dDKMsBGHzNsJBW7kBTFHY5WRfgSN94FGIhSS4Cwg1Guh8QyGugcYPA1rWGSBt+xEYWkbDKWsYx9P3rYOjojW9g3mWxyzxXEpPP2WTNmH75/W4pVNgC6i4RgiR9xZjY8uPxAOWxp/vvvKznTCkDAoVkBSnnKaUdzK1IST4zl7ZMrHJqkXlEB2Oeszoo6drxMVa+YKAgqzlw8n6wMPC86Drt+ayWMOI6Nhme7QgoIW8qcZ+ZFKOCvr0iTCCZ37YNmNQm0iG8GUP5OVFCrO4TYyNNfaXR6WEANZp6W97OtqoLfag9Ds64tJFQvD/Vvm0Vc0zaCmtM/EopozaLt40lCdy0Ob/1UJ8MPKj8HoaGw6cIhPYY25UxgXuusztPn6aDl1l4H0VTFgorX5WBBP2ahh6mCoge6/sIrmGOMPs5avJoueZLVe2PoStWrhUCPwl/g8qKaSN9pk8iG9XiIHTwmpG/HKxYZu98+hbROW04qYsZe+BBM/6SsDiDswKjTfb7PA0MoqDeXo3ORKHCH6tfVX3IbmnNmBHDNkZw5c2gRGdGBlAiTBk4NxGsckf/Y7T21cZ5rBevHxkZSVH5JLol4tSZR5b2Z7LEL7frQLfPmYRD7Gly+P9YdvKKhM1atMnPYnpk8YCisn+eid0Tc1teBCSwYOJuxdA+Lws61ba6qnZJnr0Dz8gGWzgu6V3uIS1wvfO3PXgPJnHdiyhlCyJhNLLfBZISjO6YUloHEJ+Yk2d1jKr00SC+chHvMR1nq9lCevlJWBzsd2w6EztFG+8DOgbonU349zCk8tmrLPXuq7RCUuhd2Wl6RUuqVAOfZVekks+iZ8WOwNJZC0Piz9PL9CUMCVrfObaF/Y10zLVBHMvhiK8fP4CFR9ROKmW9ZROT3MYIJg2zOqMDHyeE9fHCUo4Hr5crCnVAO2Rg0NgM5MJaAZPcrQG+8zMoHWx+ImArwz22TPhma1v/y1ouniN81ovcQNJESVdo3Nwi3MJZQDM/VSkbWNnJBuaRqYqzxPiw+iMNcZdI2TuRT9850HlkDzZI6NrCI8x/7JqO6WT3XNYi4dGidzuW7XvpaY1OvFgkUvqGnrwyhSI7KfDZRXbKy4oVPi5ex1spFR/ehmQXKysImj0YcmX22O39wz1qPXi1EdV1igBVPYkpKbHwvDV9P6Xc4ta27iGR8Buc/Vx6D+aQutHlOuMRQ0r3YIku232teAoVymMBw7Z7KRB1bpYxYsEY4KrheK941pACq7F2TefnWziecGjKw3MUnBnKSus+zGRraRxtrtBJTEkE7qg4X49pusAEPvtF7qHbJY4pu7rEQTLhDpwI5F5QZQfIxSqd7C2u7Fpr60KocG6l1u6sCQP3C4dbdgyBGV8mxkHJhy7avwgAi2Q113NZjNsqo52OMg/muZGZgxHefFT7KyHPKQoY4hHwMfqQ7doA5fA4bsxyKoUNDw9TSMarkZuGTGgbNxiViSbmh3kFIdIF/kZR9bMD7WdS8qCuH03PgbLOV/B4RS3Q0gcJfuhnKgdRiz35J6P6b1IqTOZRbBclgXi4gQzqGanZWToV78Y3kDtzFxiL9E6IpTPPlGEIqBJUpUYaHh652xOhAybkNCk6XN0MciNSOmrQe/9diS+bCrmCpQrUF7QE7kGOhV8ReOD5ZGRq+Xm1JSCxvMK2Y+lDE11Fb+G7ktF5r4pPPzdMtF9qgsiWBkzhCZ1pibprldds7CBYpiFYiFEX+JSb7oHLdrWphV336VyBRBNjeBgQF1AyMozhMy4i8YDutcyZcrys2LZi4f5hlGSg06allaj21iCh0VJ33MZiMT1G5H55Ldgdn1jItumAb3gwmpCNt/RjCjhAtGKj2ZZSMrzWDaqYuPJObs8H/bR7dXWXdhzpMYdNgMgIaGp1uesdRlQ3TMxfSKymaNDSZzWeGMnR1FmSdtB90S1ktN4ULOjMN7JB3r5ivlv7W51rFDVoQgFZERf0H75bVTH7gSYmwuYV0FRhh9meGxejiryNlGNmCBbQhH49DMHr9YTvOFsdbNOwPGOZVR2y9m3fJsI5/PAqgoMXjfGMEsqgxG8MwYaGh2U6seUTZyi7H2I7XdZ3GvW96D2Zq3UD96aMouzcxGOgSXrfNMBYBZ+lhMasprVsvHFJideRdSN6swSyy9iQEmGxa0kQWtFxsYdK/E08yad700JwoOqlJcjaG5KPyP4ZjBQz2UD5ZhVsZ6vRjDLEH6n3GY++2K7xhimUBcMOTnZxn8KF+kZa8CgjHRy6PDWDLA2EnJl4gTf4lRHxO2XaUcMPdf54Ax9Z+HjYzrxRrs8QHzxWnmkqOoU78qbGRPMI8N+xoDcMhRBO9YTDyZ4bj2YQAs1lxPjjmKer3YkCA5s+YvC02HHEXyW2a4i90uuvyE5pfUGZccxUivF05Gr486w1I0a1R0fo4ibulAHUbZyAz/qABF00RViqbNBKix6BxzFGlDxmubn0NotYCnlR5Nm3FW04sOOYo0LCBf+OcVYPjMRFXGGcNsrgmFOOQoXvQxvj8BzGZDwcf8AQxjOAed6socchSRjy1MdmUF+Tg0PF1NzjmKr61AcoTlmdxdTaDruToBXXIUKfhC9gvFX9ho7E7AR49m4OqedcpR1GdvTB34mCKbezYLKgWgm+PcKUfx2kZ2w4KOcwOYfsVX3EMaTjmKdNjXa8djb7I1pPE4ZeEdt2CTU45ihPZ+xyPnLqW+rKW8Zv3tjK25DwM65ShGOv7iPDL2MOADoUriFqB1ylGkmLjVRq5omCVAe2w9RptJJB8opbDiqzp0jt/yylF89cnrNoTOMWUootD5Y+wwwKSGWjGukhqEvPBkhxzF+CEmzgAC4OuTGqitVUkNl3ST2l6GmdKiEfTIUSQdxhGNEIZ0E2ppZboJTrRl7bE/14lAwitH0T0dyp4IFGMiUJUJIDBFq9bqVSlaZFEJ1xxF4aiQ0cDYUrTimhQtfEd9txoLJs8BCxAeOYruLBnBLI3Jc2FN8px6C9Iaq7NuLmmNgdbHnHIUbXuqqsEY0hrRk1iT1khkSTiNwom8yBcL3eUoOhOoS1Xn9F7Kr004JWKlAguPHEUPMDaD2ZoKbEvSjiBJ2ydH0QtL+2RpjOWgA2v6/HEdtH1yFH3QrI0ORWv6vGVjA+hgh5VPjqIPlQvY2LHWjQ3mLSc4cXpWv2VNjqIjyal5y0lk23KCm4G+vIGqNkeRR4Q/WIVm5R83AxkDE5ZtWjww9TmKTDDwvWxkPSVqZuUsxg10PDD1OYpMgv15b5aIAm2gs3SKcWsjE01djiKP8HvFHv3Spn61RXhghI2bTllkyFFkgQkowGRxLTA2nVpjm3Yy5SiywND0OO89riH7dmCoHDZq8w9ou4NhyVG0wIDHgWe8acO/Egs17oPMRFOngKUyoZPTPcBYcxStYPBukMLIlKNQb6G3bdQWjMMNasmeo2gBg5tt5Gpj2SwGn3EON8AA+Kf3eRCWHEUrGBiY9xMeXmmqJeIcO6FG7i30GxprjqKNYHLi6V3G6tHEDDBmZ+sbgUe1eI2MNUfRSkLs9sqZa6iGjmqxn3eDF8PRabPn9vFgMHIUzZ2Iv99HVFz1nkT9R4+2i3IW5sXEZx9rzMtRNIEhn8fqZOq/WNvilSkmdWDg4KmIYbNcV8PIUTTVSecPbyyeuMj54CnekWB3aJg5ivVYgqAzQ/vPYilFbkeCQXNSdZ0Bd83wchTrqZycy64aZDMWdVgbtxa0jezH6N1Uw8tRrKfSGDuEeqHWg8EPc3RDcQvWoX1zAO6amDmK9STf9mTWmXiOOuAwcUmKIFqeWEdPKjpOs0ClmzOrQU5B+baBXE1CTkjB4+hJRXnsAAZS2IRQyh2L1CmioCcvpzHTE+d7KCjzuFZFp3FCWq/jca2Q37Ge17uU79FYnJj1lE7Yp1xF4XGUk/bAFwBk8OSno+pzKxj/g3QF64jjM5gQjjhOCQ+LkFcsx5DjgqYD61wdOOLYc2gYh09rMOTxn7xtq3OfKkkdPq3CJZwTz+HwaT8qhyZbhPXW6x0cmiZwLDgNzdWx4JcCL0xV6GPB6cvWo8fxx9tUQmnTmYfmtMvbKqHOigPbb/8C8jiwvXxy3mGlQleiwaP0T7pjHPCwj9LnFwu/Tl84Sp/QvDtfcqBkuPmSgzi0eJLuCobTu96/cMmBvn5iYz0f+IJF8dhzI6uvnwijy6pnYonCzZeun1DeeIeLQUKV50/qdu3FIKEKrzrcl/bli0FIexZ0Eq0ON5uh6MhMdH4dKRNE/egi2Fe26IrpyhYHbblmgILg45+yvrgd2RypQ+P/fTiYMEYwMvvRa44iEJZZM7c3Xl1A9TNgwiYvoMKFU/xzsAcaJJDY/5q7GkzgpW1BYT7U7lkE1hgGYmQjtx0qj57YmtKen0jzrbC7ld1IXXQYK1n3zBvcInVYPCVEzpNGLzoEogDw4vuvoFx4u+IMYCSxaB3seCqcSxXqctAGZxiSvrZ1qL1XzwSjPXbDdxk849pWcX+h7lPBkGGEF+rKmo0LXyHUoG+uOn4eA7i96tjB4cMmcb6Eeoe+zuh8gVaTODQfC0N9CbVg7Rn0J7oenERoo3L0UuKTrwe/ofPF7c0OjU4jiBee10x5EAz6qo+KdOMXt4OK3F8Fja/5WkINtp1DCKJhbQ2KO+TthnRkHqEUk8navOnYi4brRDqEEhoBQ5ZSe40+ozuhww9QX7/Al5N1+6r47yQ8YWb9AhldsXK3hBHfZRirx89bbPYvCOXb5tctGLQMkqJLt31qf7EDmGsXc69bJEIlnPwAaYdrZzujxUPXtXJVAwKisnCHM/C43zunvxFLcD4KIMiKwfCoJr/DmqEnj8NBkem4WzPWsQcaCEZKvUkizV/nar6xRwZm1/w1T6lvwEDnpZA8B45QO1BQFZTL4q1vPlXhjjb9twKu4NQ3Hv4AF7sCc/+GbC1XswOLA8SH2WrZqjjO65dRZ1fM+vPJpnd8mHHRsbeZzPuzYucQYfsx0jauyNJtka9n09fuYNB/eekPBt3X6WydF9s0E9dP/l66n/ml2MharXaStFutTF6/r1bb7yXFV4kh1TaUPlVx9t8M5xLBVLPoPkB75c/75UACnZetm3nf3vNrcX70j/7oj/7oj+7pf9jr6bckHXYyAAAAAElFTkSuQmCC";
            String base64Image = data.split(",")[1];
            byte[] imageBytes = DatatypeConverter.parseBase64Binary(base64Image);
            emitter.image(roomService.roomName, imageBytes);
        }).start();


    }

    private void emitScreen() {
        try {
            emitter.image(roomService.roomName, imageByteArray);
            Thread.sleep(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Tworzy zrzut obrazu i wywoluje metody je przetwarzajace
     */
    private void screenCapture() {
        screen = robot.createScreenCapture(rectangle);
        scaleImage(zoomValue);
        convertToArray();
    }

    /**
     * Skaluje obraz
     *
     * @param procent wartość procentowa jak ma byc przeskalowany obraz
     */
    private void scaleImage(double procent) {
        int width = screen.getWidth();
        int height = screen.getHeight();
        procent = procent / 100;
        AffineTransform scale = AffineTransform.getScaleInstance(procent, procent);
        BufferedImageOp op = new AffineTransformOp(scale, AffineTransformOp.TYPE_BILINEAR);
        BufferedImage filteredImage =
                new BufferedImage((int) (width * procent), (int) (height * procent), screen.getType());
        op.filter(screen, filteredImage);
        screen = filteredImage;
    }

    /**
     * Zamienia obraz na tablicę
     */
    private void convertToArray() {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            writeJPG(screen, baos, typeQuality.getQualityValue());
            imageByteArray = baos.toByteArray();
            baos.close();
        } catch (IOException ex) {
            Logger.getLogger(ImageScreenShot.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Zapis obrazu do JPG
     *
     * @param bufferedImage
     * @param outputStream
     * @param quality
     * @throws IOException
     */
    public static void writeJPG(BufferedImage bufferedImage, OutputStream outputStream, float quality) throws IOException {
        Iterator<ImageWriter> iterator = ImageIO.getImageWritersByFormatName("jpg");
        ImageWriter imageWriter = iterator.next();
        ImageWriteParam imageWriteParam = imageWriter.getDefaultWriteParam();
        imageWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        imageWriteParam.setCompressionQuality(quality);
        ImageOutputStream imageOutputStream = new MemoryCacheImageOutputStream(outputStream);
        imageWriter.setOutput(imageOutputStream);
        IIOImage iioimage = new IIOImage(bufferedImage, null, null);
        imageWriter.write(null, iioimage, imageWriteParam);
        imageOutputStream.flush();
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public TypeQuality getTypeQuality() {
        return typeQuality;
    }

    public void setTypeQuality(TypeQuality typeQuality) {
        this.typeQuality = typeQuality;
    }
}
