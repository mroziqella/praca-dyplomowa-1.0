package pl.mroziqella.hermes.client.core.client.listener;

import io.socket.emitter.Emitter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mroziqella.hermes.client.core.client.listener.ToView.HomeView;
import pl.mroziqella.hermes.client.core.service.UserService;
import pl.mroziqella.hermes.core.until.MapperJSON;
import pl.mroziqella.hermes.core.dto.InfoDTO;

import java.util.logging.Logger;

/**
 * Klasa pobiera informacje po zalogowaniu
 * Created by Mroziqella on 18.12.2016.
 */

@Component
public class Initialization implements Emitter.Listener {
    private Logger logger = Logger.getLogger(String.valueOf(this.getClass().getName()));

    @Autowired
    private HomeView homeView;
    @Autowired
    private UserService userService;

    @Override
    public void call(Object... objects) {
        InfoDTO infoDTO = MapperJSON.toObject(objects[0], InfoDTO.class);
        homeView.downloadInitData(infoDTO);
        homeView.initRoomNamespace(userService.getCurrentUser().getRooms());
        logger.info("...........INIT COMPLETE...........");
    }
}
