# System do obsługi pulpitów zdalnych #

### Główne cele ###

* Pozwolenie na proste udostępnienie pulpitu komputera w sieci 
* Możliwość połączenia się z systemem za pomocą różnych platform

### Schemat ###

![Obraz1.png](https://bitbucket.org/repo/a8egxK/images/1430177775-Obraz1.png)

### Główne funkcjonalności ###
* Możliwość udostępniania ekranu komputera
* Możliwość odbierania udostępnionego pulpitu, oraz sterowania nim
* Zmiana wielkości, jakości obrazu
* Zapewnienie dwóch rodzajów transmisji – prywatnej, publicznej
* Tworzenie pokoi
* Przypisanie użytkowników do pokojów
* Logowanie do pokoju z wykorzystaniem hasła
* Rejestracja w systemie
* Blokowanie użytkowników

### Składowe ###
* Serwer
* Aplikacja desktopowa
* Aplikacja webowa
* Aplikacja androidowa

### Technologie ###

* Java
* Spring
* JavaFX
* JPA
* Socket.IO
* JSP
* CSS
* jQuery