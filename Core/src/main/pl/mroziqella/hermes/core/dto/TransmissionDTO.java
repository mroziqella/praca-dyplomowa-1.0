package pl.mroziqella.hermes.core.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;

/**
 * Created by Mroziqella on 23.12.2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransmissionDTO {
    private byte[] image;
    private MouseDTO mouseDTO;
    private String keyEvent;



    public TransmissionDTO() {
    }

    public TransmissionDTO(String keyEvent) {
        this.keyEvent = keyEvent;
    }

    public TransmissionDTO(byte[] image) {
        this.image = image;
    }

    public TransmissionDTO(MouseDTO mouseDTO) {
        this.mouseDTO = mouseDTO;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public MouseDTO getMouseDTO() {
        return mouseDTO;
    }

    public void setMouseDTO(MouseDTO mouseDTO) {
        this.mouseDTO = mouseDTO;
    }

    public String getKeyEvent() {
        return keyEvent;
    }

    public void setKeyEvent(String keyEvent) {
        this.keyEvent = keyEvent;
    }

    @Override
    public String toString() {
        return "TransmissionDTO{" +
                "image=" + Arrays.toString(image) +
                ", mouseDTO=" + mouseDTO +
                '}';
    }
}
