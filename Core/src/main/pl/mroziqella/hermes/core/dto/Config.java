package pl.mroziqella.hermes.core.dto;

/**
 * Created by Mroziqella on 23.02.2017.
 */
public class Config {
    private int port;
    private boolean run;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isRun() {
        return run;
    }

    public void setRun(boolean run) {
        this.run = run;
    }
}
