package pl.mroziqella.hermes.core.until;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by Mroziqella on 08.03.2017.
 */
public class Until {
    public static String hashPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

}
