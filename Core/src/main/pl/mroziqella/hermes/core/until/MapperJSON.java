package pl.mroziqella.hermes.core.until;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


/**
 * Created by Mroziqella on 17.12.2016.
 */
public class MapperJSON {
    private static Gson gson = new Gson();

    public static JSONObject toJSONObject(Object o){
        try {
            return new JSONObject(gson.toJson(o));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T toObject(String s,Class<T> javaType){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(s,javaType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static <T> T toObject(Object s, Class<T> javaType){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(s.toString(),javaType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
