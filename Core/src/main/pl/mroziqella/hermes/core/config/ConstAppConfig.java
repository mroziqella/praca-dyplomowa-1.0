package pl.mroziqella.hermes.core.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Kamil on 2016-10-10.
 */
public class ConstAppConfig {


    public ConstAppConfig() {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            String filename = "config.properties";
            input = ConstAppConfig.class.getClassLoader().getResourceAsStream(filename);
            if(input==null){
                System.out.println("Sorry, unable to find " + filename);
                return;
            }

            //load a properties file from class path, inside static method
            prop.load(input);

            //get the property value and print it out
            System.out.println(prop.getProperty("ip"));
            System.out.println(prop.getProperty("dbuser"));
            System.out.println(prop.getProperty("dbpassword"));

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }



    }

//    public static  String HOST = "192.168.43.69";
    public static  String HOST = "192.168.1.100";
    public static  int PORT = 9093;
    public static final String LOGIN = "login";
    public static final String INIT = "init";
    public static final  String TRANSMISSION = "transmision";


}
