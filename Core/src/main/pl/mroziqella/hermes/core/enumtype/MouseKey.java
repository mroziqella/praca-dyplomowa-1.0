package pl.mroziqella.hermes.core.enumtype;

/**
 * Created by Mroziqella on 03.01.2017.
 */
public enum MouseKey {
    LEFT,
    RIGHT;
}
