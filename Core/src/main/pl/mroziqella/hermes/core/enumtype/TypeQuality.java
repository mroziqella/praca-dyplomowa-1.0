package pl.mroziqella.hermes.core.enumtype;

/**
 * Created by Mroziqella on 29.12.2016.
 */
public enum TypeQuality {
    HIGH,
    MEDIUM,
    LOW;

    public float getQualityValue() {
        switch (this) {
            case HIGH:
                return  0.9f;
            case MEDIUM:
                return  0.6f;
            default:
                return  0.1f;
        }
    }
}
